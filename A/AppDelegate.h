//
//  AppDelegate.h
//  A
//
//  Created by miaotong on 2018/7/6.
//  Copyright © 2018年 miaotong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

